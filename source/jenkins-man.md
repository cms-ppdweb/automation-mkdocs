## Adding a new group

Create Jenkins credential https://dpg-ecal-calib.web.cern.ch/manage/credentials/
* Credential ID must be set to something unique for each subsystem
* username: Service account username
* password: Service account password

Create shared library name https://dpg-ecal-calib.web.cern.ch/manage/configure

Create and assign roles in groups https://dpg-ecal-calib.web.cern.ch/manage/role-strategy/

Create node https://dpg-ecal-calib.web.cern.ch/manage/computer/ 
Remember to set ECAL_INFLUXDB_USER and ECAL_INFLUXDB_PWD

Add new node to workflow https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Tools/job/clear-suspended-nodes/configure

## Troubleshooting

### Items fail with `.git/index: index file smaller than expected`

If jobs fail with error:

```bash
stderr: fatal: .git/index: index file smaller than expected
```

the shared-lib repository got corrupted.
To fix go to *Manage>System* and in the *Global Pipeline Libraries* section find the correct Library corresponding to the project and enable `Fresh clone per build`.
Then run the jobs again. If it worked disable `Fresh clone per build` again.
