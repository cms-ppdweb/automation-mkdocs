# Mattermost notification

The framework supports sending notification through Mattermost webhooks to notify of failed runs.

To setup mattermost notification add a new [Incoming Webhook integration](https://mattermost.web.cern.ch/cms-exp/integrations/incoming_webhooks)

Copy the URL of the Webhook. It will have this format: `https://mattermost.web.cern.ch/hooks/<UNIQUE_WEBHOOK_HASH>`

Replace the url in the variable `script.env.notify_url` in `shared-lib/src/<PROJECT_NAME>/vars/GlobalVars.groovy`

Edit the Jenkins file of your workflows by adding in the pipeline:

```Jenkinsfile
    post {
      unsuccessful {
        sendMattermostNotification "ERROR"
      }
    }
```

to receive a notification in case of failed execution.
You can edit the `sendMatterostNotification.groovy` script in `shared-libs/vars/` to change the behaviour

In addition, you can add the argument `--notify=$notify_url` to the python script run in a pipeline step to receive a notification to receive Warning level logs from the TaskHandlers.