Openshift deployments
=====================

All the automation framework components (except the [influxdb](influxdb.md) instance) are
deployed through Openshift. The CERN IT provides different openshift interfaces depending
on the website tipe: [app-catalogue.cern.ch](https://app-catalogue.cern.ch), 
[paas.cern.ch](https://paas.cern.ch) and [webeos.cern.ch](https://webeos.cern.ch).
The automation applications are deployed from the three interfaces mentioned above
following this scheme:

- Jenkins: [paas.cern.ch](https://paas.cern.ch)
- Grafana: [app-catalogue.cern.ch](https://app-catalogue.cern.ch), [guide](https://grafana.docs.cern.ch/)
- Grafana image renderer: [paas.cern.ch](https://paas.cern.ch), [guide](https://grafana.docs.cern.ch/2._Configuration/4-configure-image-render-plugin/)
- Documentation (this website): [webeos.cern.ch](https://webeos.cern.ch), [guide](https://abpcomputing.web.cern.ch/guides/mkdocs_site/)
