Automation database (influxdb)
==============================

The entire automation framework relies on a database to save all the relevant workflows/jobs configurations and status.
The influxdb that support the framework is hosted by the CERN IT on the [dbod platform](https:dbod.web.cern.ch). The original request for the database creation can be found in this [ticket](https://cern.service-now.com/service-portal?id=ticket&table=e&n=RQF1758809). The ticket contains useful links to the dbod user guide. The influxdb instance has been setup to perform daily backups.

## InfluxDB admin tasks

### Connect to the db
The influxdb pyhton client is used to perform admin tasks through a python interactive session.
Some of the low level actions can be performed using only the influxdb python package. Nonetheless, it is recommended to use the automation-control environment. 

```python
import influxdb

client = influxdb.InfluxDBClient(
            host="ecal-automation-relay.web.cern.ch",
            port=80,
            username="admin",
            password="ADMIN-PASSWORD",
            ssl=False,
            database="")
```

### Create new database

Create the database itself
```python
client.create_database('db_name')
```

Create new users if necessary. Usually separate between a READ only user and a READ/WRITE one.
```python
client.create_user('writeuser', 'writepassword')
client.create_user('viewuser', 'viewpassword')
```

Grant privileges to the users:
```python
client.grant_privilege('READ', 'db_name', 'viewuser')
client.grant_privilege('ALL', 'db_name', 'writeuser')
```

??? Note 
    The influxdb users are completely independent of CERN accounts. One might choose to have the write user named as the service account used
    to access lxplus from Jenkins for convenience, but the two would still be independent.
    

### Manually set tasks status
Changing the status of tasks require using the RunCtrl class. In order to be able to write the new status the influxdb username and password should
be set to an account that as WRITE access to the db being used. This can be done setting the `ECAL_INFLUXDB_USER` and `ECAL_INFLUXDB_PWD` environment variables.

```python
from ecalautoctrl import RunCtrl

rctrl = RunCtrl(dbname="db_name", campaign="campaign_name")
rctrl.updateStatus(run=RUN_NUMBER, status={'task1-name':'done', 'task2-name' : 'done'})
```

The status of multiple tasks can be updated at once for a given run as shown above.
