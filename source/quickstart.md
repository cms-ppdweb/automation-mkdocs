# QuickStart guide

## Clone the repository

First duplicate or fork the Sample Automation repository in https://gitlab.cern.ch/cms-ppdweb/automation .
This will give you the structure of the repository to modify for your use-case.

!!! note
    It is strongly advised to keep the repository public.

## Obtain service account

To run the jobs on HTCondor you will need a Service Account with [robot certificate](https://ca.cern.ch/ca/Help/?kbid=021003) and added to the `zh: Linux group zh - CMS` (https://resources.web.cern.ch/resources/Manage/Linux/Subscribe.aspx?login=service_account) and the CMS VO by [Adding the certificate](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideLcgAccess#How_to_register_in_the_CMS_VO:~:text=currently%20(June%202024)%20Robot%20certificate%20need%20special%20care) to the owner's account.


The service account can be requested at https://account.cern.ch/account/Management/NewAccount.aspx under "New Account" > "Service" by providing the relevant information.  

Download the p12 file and copy it to the lxplus service account home directory. Then generate the private/public key pair using the commands in: https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookStartingGrid#ObtainingCert

Generate the grid certificate if needed, saving it in home:

```
voms-proxy-init -voms cms --out ~/grid_proxy.x509 --valid 192:00
```

Generate the keytab in the service account home directory:
```bash
ktutil
addent -password -p <SERVICE_USERNAME>@CERN.CH -k 1 -e RC4-HMAC
wkt /afs/cern.ch/user/<S>/<SERVICE_USERNAME>/.globus/<SERVICE_USERNAME>.keytab
l -e
```


## Create Jenkins admin account

Request Jenkins credentials for the project (agent name, credential id, shared library name).

Get Jenkins admin access rights for editing Jenkins items.

## Add credential to the repository

Add the service account credentials in the CI/CD Variables tab:

in you repository, go to **Settings > CI/CD > Variables**.
Add the Variables: 
 - SERVICE_USERNAME
 - SERVICE_PWD

The username should match the Service account username.  
The Password is a new gitlab access token that can be created from the Service Account profile with permissions:  
`read_api, read_user, read_repository, read_registry`

## Build first docker image

Go to **Build > Pipelines > Run Pipeline**

Run the pipeline on master (default settings should be fine)
Make sure that the `build_docker_dev` passed, from the **Build > Jobs** tab.

## Unpack the docker image

From the **Deploy > Container Registry** tab, open the registry corresponding to your repository and search for `dev`. Click on the clipboard button next to the name to copy the full path.

Add the full path to https://gitlab.cern.ch/unpacked/sync/-/blob/master/recipe.yaml?ref_type=heads by opening a pull request to the [repository](https://gitlab.cern.ch/unpacked/sync).

In more or less half an hour your image will be unpacked in:
`/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/<YOUR-REPOSITORY-IMAGE>:dev`

## Configure repository

Change name of `sharedlibs/src/[...]/vars` folder with the Jenkins credential id from step [Create Jenkins admin account](#create-jenkins-admin-account)

!!! By default golbal vars will always be read from master

Update the `automationApptainerStep.groovy` in `automation/shared-lib/vars` adding the Jenkins credential id.

Update the `automationApptainerStepNoCampaign.groovy` in `automation/shared-lib/vars` adding the Jenkins credential id.

## Create custom workflow

In the `workflows` folder add a new directory with the name of the workflow you want to create. The folder `template-workflow` shows an example of it.

The folder must contain a Jenkinsfile with proper shared library name, agent name, and workflow dir in each stage.

See [New workflow](new_wflow.md) for additional information.

## Add campaign to database

Login to `lxplus` with the service account.

Run the following commands to launch and configure the Apptainer image:

```bash
singularity shell --cleanenv -B /cvmfs -B /eos /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/<YOUR-REPOSITORY-IMAGE>:dev
export ECAL_INFLUXDB_USER=<INFLUXDB-EDIT-USER>
export ECAL_INFLUXDB_PWD=<INFLUXDB-EDIT-PASSWORD>
source /home/<SERVICE-USER>/setup.sh
```

Add a campaign with the command:

```bash
ecalrunctrl.py --db '<INFLUXDB-NAME>' create --campaign 'test_campaing'
```


## Assign workflows to campaign and injecting run
Add workflows to the campaign:

```bash
ecalrunctrl.py --db '<INFLUXDB-NAME>' rtype-update --campaign 'test_campaing' --add <WORKFLOW-NAME> --type=all
```

Inject a new run:
```bash
ecalrunctrl.py --db '<INFLUXDB-NAME>' inject --campaign 'test_campaign' --era 2023B --runs 366504,366499  --globaltag='130X_dataRun3_Express_v2'
```


## Create Jenkins Item
Access the [Jenkins dashboard](https://dpg-ecal-calib.web.cern.ch/) and go to the View corresponding to your group. 
Hit `+ New Item` to start the creation of a new item in Jenkins.

Enter an item name that start with the name of your group, as set up in step [Create Jenkins admin account](#create-jenkins-admin-account) (i.e. `ecal-sample-workflow` for ECAL).

At the end of the form enter `sample-workflow` in the Copy from field and click Ok to continue.

In the new form, change the fields:
* Repository URL with the https url of your repository (ending in `.git`)
* Credentials selecting from the dropdown menu the credentials setup in the [Create Jenkins admin account](#create-jenkins-admin-account) step for your group
* Sparse Checkout paths Path with the workflow folder corresponding to the workflow created in step [Create custom workflow](#create-custom-workflow) (i.e. `workflows/template-workflow`)
* Script Path with the path to the corresponding Jenkinsfile (i.e. `workflows/template-workflow/Jenkinsfile`)

Save the changes, enable the project and run the build to check if everything went smoothly. 
